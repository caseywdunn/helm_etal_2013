# Additional Files - Helm *et al.* 2013

This is a repository of the additional files for the manuscript:

> Helm, RR, S Siebert, S Tulin, J Smith, CW Dunn (2013) Characterization of differential transcript abundance through time during Nematostella vectensis development. BMC Genomics. [doi:10.1186/1471-2164-14-266](http://dx.doi.org/10.1186/1471-2164-14-266)


Additional files 1, 2, 3, and 12 were zipped when provided with the manuscript.