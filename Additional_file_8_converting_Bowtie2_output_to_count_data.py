#!/usr/bin/env python	
import sys
from collections import defaultdict


Usage = """
From: "Helm et al. - Characterization of differential transcript abundance 
through time during Nematostella vectensis development"

This program takes a sam file generated by bowtie2, and writes out the number 
of reads that map to each reference sequence. Reads that map to more than one 
reference are not counted at all. If a read maps more than once to the same 
reference sequence, it is only counted once.

Usage:

bowtie_map_to_counts.py bowtie.sam > bowtie.counts 

"""

if len(sys.argv) < 2:
	print Usage
else:
	map_name = sys.argv[1]
	
	#A dictionary with key read and value set of genes that the read maps to
	map = defaultdict(set) 
	map_handle = open(map_name, "rU")
	
	# Loop over the lines, and build a map of all the references that each 
	# read hits
	n = 0
	for line in map_handle:
		if line[0] == '@' :
			continue
		n = n + 1
		line = line.strip()
		fields = line.split('\t')
		if len(fields)<3:
			raise ValueError("Read number {0} is poorly formed:\n  {1}".format(n, line))
		read = fields[0]
		ref = fields[2]
		map[read].add(ref)
		#if n % 100000 == 0:
			# print n, "lines read"
	
	# Go through the read map, and convert this to counts for each reference 
	# sequence
	
	counts = defaultdict(int)
	
	for key, hits in map.iteritems():
		# Check to see if the read mapped uniquely, if not skip it
		if len(hits) > 1:
			continue
		
		ref = list(hits)[0]
		counts[ref] = counts[ref] + 1
		
	# Print out the results
	print "reference\tcount"
	for ref, count in counts.iteritems():
		print ref + "\t" + str(count)
