# The shell calls for Bowtie2 mapping.

bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH020-0/*.fastq -S RRH020-0_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH021-6/*.fastq -S RRH021-6_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH022-12/*.fastq -S RRH022-12_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH023-24/*.fastq -S RRH023-24_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH024-5/*.fastq -S RRH024-5_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH025-10/*.fastq -S RRH025-10_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH040-0/*.fastq -S RRH040-0_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH041-6/*.fastq -S RRH041-6_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH042-12/*.fastq -S RRH042-12_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH043-24/*.fastq -S RRH043-24_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH044-5/*.fastq -S RRH044-5_bowtie.map
bowtie/2.2.0beta3/bin/bowtie2 --phred33 --very-sensitive-local -a -x REFERENCE_TO_MAP -U Sample_RRH045-10/*.fastq -S RRH045-10_bowtie.map